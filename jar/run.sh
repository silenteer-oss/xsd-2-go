#!/usr/bin/env bash

 xsd=$1
# xsd="Selektivvertragsdefinition_V18.xsd"
 xjc -extension -quiet -no-header $xsd
 cd ./generated
 javac *.java
 cd ..
 java -jar xsd2go.jar ./generated/ generated >> test.go
 rm -rf ./generated
