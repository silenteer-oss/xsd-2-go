package translator;

import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.net.URL;

import org.junit.Test;

public class TestTranslator {

	@Test
	public void testTranslator() throws ClassNotFoundException {

		String pkg = "generated";
		String folder = "/Users/hung/project/silenteer/xsd-2-go/src/test/resources/generated/";
		Translator t = new Translator(folder, pkg);

		String src = t.go("generated.Config");
		System.out.println(src);

	}

	@Test
	public void testTranslator2() throws ClassNotFoundException {

		String pkg = "generated";
//		String folder = "/Users/hung/project/silenteer/xsd-2-go/src/test/resources/generated/";
		String folder = "/Users/hung/project/silenteer/xsd-2-go/generated/";
		Translator t = new Translator(folder, pkg);

		String src = t.go("generated.Os");
		System.out.println(src);

	}


	@Test
	public void testTranslator1() throws ClassNotFoundException {

		String pkg = "generated";
		String folder = "/Users/hung/project/silenteer/xsd-2-go/generated/";
		Translator t = new Translator(folder, pkg);

//		String src = t.go("generated.WochentagTyp");
		String src = t.go("generated.Selektivvertragsdefinition");
		System.out.println(src);

	}

}
