package loader;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import org.junit.Assert;
import org.junit.Test;

public class TestLoader {

	@Test
	public void testLoader() {
		String folder = "/Users/hung/project/silenteer/xsd-2-go/src/test/resources/generated/";
		Loader loader = new Loader(folder);

		Assert.assertTrue(loader.getClassFullNameTable().contains("FlashVariables$Value"));
		Assert.assertTrue(loader.getClassNameTable().contains("FlashVariablesValue"));

	}

	@Test
	public void testLoader2() throws ClassNotFoundException {
		String folder = "/Users/hung/project/silenteer/xsd-2-go/generated";

		File file = new File(folder);

//		ClassLoader classLoader = new CustomClassLoader();
//		Class<?> aClass = classLoader.loadClass("generated.MetainformationSimpleTyp");
//		System.out.println(aClass);

		//		Loader loader = new Loader(folder);
//		loader.load("generated.FeiertagTyp");
	}
}
