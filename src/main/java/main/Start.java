package main;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import translator.Translator;

public class Start {

  public void run(String folder, String pkg) throws ClassNotFoundException {
    Translator t = new Translator(folder, pkg);
    File dir = new File(folder);

    for (String fname : dir.list()) {

      if (fname.endsWith("package-info.class")) {
        continue;
      }

      if (fname.endsWith(".class") ) {
        fname = fname.replaceAll("\\.class", "");
        String src = t.go(pkg + "." + fname);
        System.out.println(src);
      }
    }

    for (String temp : t.getEnumClassTable()) {
      System.out.println("\n//-----------------------------------------------");
      System.out.println(t.enumValues(pkg+"."+temp));
    }
  }

  public static void main(String[] args) throws ClassNotFoundException, MalformedURLException {

		String classDir = args[0];
		String pkg = args[1];

//    String classDir = "/Users/hung/project/medi/tutum/ares/resources/kts/build/classes/java/main/ehd/kts/_001";
//    String pkg = "ehd._001";

//    String classDir = "/Users/hung/project/medi/xsd-2-go/ehd/kts/_001/";
//    String classDir = "/Users/hung/project/medi/xsd-2-go/ehd/_001";
//    String pkg = "ehd.kts._001";

//    String classDir = "/Users/hung/project/medi/tutum/ares/resources/generated";
//    String pkg = "generated";


    Start s = new Start();
    s.run(classDir, pkg);

  }

}
