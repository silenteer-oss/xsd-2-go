package loader;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class CustomClassLoader extends ClassLoader {

  private String path = ".";

  public CustomClassLoader(String path) {
    this.path = path;
  }


  @Override
  public Class findClass(String name) throws ClassNotFoundException {
    byte[] b = loadClassFromFile(name);
    return defineClass(name, b, 0, b.length);
  }

  private byte[] loadClassFromFile(String fileName) throws ClassNotFoundException {
    String className = fileName.substring(fileName.lastIndexOf(".") + 1);
    String filePath = this.path + File.separatorChar + className + ".class";

    try (InputStream inputStream = new FileInputStream(filePath)) {
      ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
      int nextValue;
      while ((nextValue = inputStream.read()) != -1) {
        byteStream.write(nextValue);
      }
      return byteStream.toByteArray();
    } catch (FileNotFoundException e) {
      throw new ClassNotFoundException(e.getMessage());
    } catch (IOException e) {
      throw new ClassNotFoundException(e.getMessage());
    }
  }
}
